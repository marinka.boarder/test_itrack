<?php
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("MyClass", "OnBeforeIBlockElementAddHandler"));
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("MyClass", "OnBeforeIBlockElementUpdateHandler"));
class MyClass
{
    function OnBeforeIBlockElementAddHandler(&$arFields)
    {
        /*посчитаем средние доходы и расходы при добавлении элемента*/
        if($arFields["IBLOCK_ID"] == 1){
            $properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arFields["IBLOCK_ID"]));
            while ($prop_fields = $properties->GetNext())
            {
                if($prop_fields["CODE"] == 'PROFIT'){
                    $profit_id = $prop_fields["ID"];
                    foreach ($arFields["PROPERTY_VALUES"][$profit_id] as $prof){
                        $profit_count = $prof['VALUE'];
                    }
                }
                if($prop_fields["CODE"] == 'M_PROFIT'){
                    $mprofit_id = $prop_fields["ID"];
                }
                if($prop_fields["CODE"] == 'EXPENSES'){
                    $expenses_id = $prop_fields["ID"];
                    foreach ($arFields["PROPERTY_VALUES"][$expenses_id] as $exp){
                        $expenses_count = $exp['VALUE'];
                    }
                }
                if($prop_fields["CODE"] == 'M_EXPENSES'){
                    $mexpenses_id = $prop_fields["ID"];
                }
                if($prop_fields["CODE"] == 'CITIZEN'){
                    $citizen_id = $prop_fields["ID"];
                    foreach ($arFields["PROPERTY_VALUES"][$citizen_id] as $cit){
                        $citizen_count = $cit['VALUE'];
                    }
                }
            }
            $mprofit_count = $profit_count / $citizen_count;
            $mexpenses_count = $expenses_count / $citizen_count;
            foreach ($arFields["PROPERTY_VALUES"][$mprofit_id] as $key => $prof){
                $arFields["PROPERTY_VALUES"][$mprofit_id][$key] = array('VALUE'=>$mprofit_count);
            }
            foreach ($arFields["PROPERTY_VALUES"][$mexpenses_id] as $key => $exp){
                $arFields["PROPERTY_VALUES"][$mexpenses_id][$key] = array('VALUE'=>$mexpenses_count);
            }
        }
        return $arFields;
    }
    function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {
        /*пересчитаем средние доходы и расходы при изменении элемента*/
        if($arFields["IBLOCK_ID"] == 1){
            $properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$arFields["IBLOCK_ID"]));
            while ($prop_fields = $properties->GetNext())
            {
                if($prop_fields["CODE"] == 'PROFIT'){
                    $profit_id = $prop_fields["ID"];
                    foreach ($arFields["PROPERTY_VALUES"][$profit_id] as $prof){
                        $profit_count = $prof['VALUE'];
                    }
                }
                if($prop_fields["CODE"] == 'M_PROFIT'){
                    $mprofit_id = $prop_fields["ID"];
                }
                if($prop_fields["CODE"] == 'EXPENSES'){
                    $expenses_id = $prop_fields["ID"];
                    foreach ($arFields["PROPERTY_VALUES"][$expenses_id] as $exp){
                        $expenses_count = $exp['VALUE'];
                    }
                }
                if($prop_fields["CODE"] == 'M_EXPENSES'){
                    $mexpenses_id = $prop_fields["ID"];
                }
                if($prop_fields["CODE"] == 'CITIZEN'){
                    $citizen_id = $prop_fields["ID"];
                    foreach ($arFields["PROPERTY_VALUES"][$citizen_id] as $cit){
                        $citizen_count = $cit['VALUE'];
                    }
                }
            }
            $mprofit_count = $profit_count / $citizen_count;
            $mexpenses_count = $expenses_count / $citizen_count;
            foreach ($arFields["PROPERTY_VALUES"][$mprofit_id] as $key => $prof){
                $arFields["PROPERTY_VALUES"][$mprofit_id][$key] = array('VALUE'=>$mprofit_count);
            }
            foreach ($arFields["PROPERTY_VALUES"][$mexpenses_id] as $key => $exp){
                $arFields["PROPERTY_VALUES"][$mexpenses_id][$key] = array('VALUE'=>$mexpenses_count);
            }
        }
        return $arFields;
    }

}
?>