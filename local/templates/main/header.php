<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Localization\Loc;
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?$APPLICATION->ShowTitle()?></title>
    <?$APPLICATION->ShowHead();?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/bootstrap.css');
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/bootstrap-theme.css');
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/style.css');

    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/jquery-3.1.0.min.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/bootstrap.min.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/main.js');
    ?>
    <link rel="icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico">
</head>

<body>

<?IncludeTemplateLangFile(__FILE__);?>
<div id="panel"><?($APPLICATION->ShowPanel())?></div>

