<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$i = 1;
?>
<div class="city-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;

?>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Название</th>
            <th scope="col">Доходы общие</th>
            <th scope="col">Расходы общие</th>
            <th scope="col">Количество жителей</th>
            <th scope="col">Место в рейтинге <br>по количеству жителей</th>
            <th scope="col">Место в рейтинге по средним <br>доходам населения</th>
            <th scope="col">Место по средним <br>расходам населения</th>
        </tr>
        </thead>
        <tbody>
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <tr class="news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <td><?=$arItem["NAME"];?></td>
                    <?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
                        <td><?=$arProperty["DISPLAY_VALUE"];?></td>
                    <?endforeach;?>
                    <td><?=$i++;?></td>
                    <td><?=$arItem["PROPERTIES"]["R_PROFIT"]["VALUE"];?></td>
                    <td><?=$arItem["PROPERTIES"]["R_EXPENSES"]["VALUE"];?></td>
                </tr>
            <?endforeach;?>
        </tbody>
    </table>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
