<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach($arResult["ITEMS"] as $key => $arItem){
    $r_pro = 0;
    $r_exp = 0;
    foreach($arResult["ITEMS"] as $key2 => $arItem2){
        if($arItem["PROPERTIES"]["M_PROFIT"]["VALUE"] <= $arItem2["PROPERTIES"]["M_PROFIT"]["VALUE"])
            $r_pro++;
        if($arItem["PROPERTIES"]["M_EXPENSES"]["VALUE"] <= $arItem2["PROPERTIES"]["M_EXPENSES"]["VALUE"])
            $r_exp++;
    }
    $arResult["ITEMS"][$key]["PROPERTIES"]["R_PROFIT"]["VALUE"] = $r_pro;
    $arResult["ITEMS"][$key]["PROPERTIES"]["R_EXPENSES"]["VALUE"] = $r_exp;
}
//Bitrix\Main\Diag\Debug::writeToFile(array('$arResult["ITEMS"]' => $arResult["ITEMS"] ),"","debug.txt");
?>